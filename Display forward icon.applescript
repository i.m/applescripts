if application "Spotify" is running then
	tell application "BetterTouchTool"
		set RewindIconPath to POSIX path of (path to resource "icons/linea/music_fastforward_button.png")
		return "{\"text\":\" \", \"icon_path\":\"" & RewindIconPath & "\"}"
	end
else
	return ""
end if