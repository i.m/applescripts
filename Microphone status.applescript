set microVolume to input volume of (get volume settings)
set status to ""
if microVolume > 0 then
  set status to "ios-mic.png"
  set volume input volume 100
else
  set status to "ios-mic-off.png"
  set volume input volume 0
end if
tell application "BetterTouchTool"
		set statusIconPath to POSIX path of (path to resource ("icons/ioniconpng/" & status))
		return "{\"text\":\" \", \"icon_path\":\"" & statusIconPath & "\"}"
end