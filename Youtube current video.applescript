set str to "" as string
if ((str is equal to str) and (application "Safari" is running)) then
	tell application "Safari"
		repeat with t in tabs of windows
			tell t
				if URL starts with "https://www.youtube.com/watch" then
					set state to do JavaScript [�
						"document", �
						".querySelectorAll('div[class*=\"-mode\"]')[0]", �
						".className", �
						".match(/(playing|paused|ended)-mode/)[1]"] �
						as text
					if state = "playing" then
						set tmp to name of t as string
						set AppleScript's text item delimiters to " - Youtube"
						set str to text item 1 of tmp
						set AppleScript's text item delimiters to ""
					end if
				end if
			end tell
		end repeat
	end tell
end if
return str