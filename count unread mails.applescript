set status to application "Mail" is running
tell application "Mail"
	set nb to unread count of inbox
	if nb is greater than 0 then
		if not status then
			quit
		end if
		return nb
	end if
	
end tell
return ""