if application "Safari" is running then
	tell application "Safari"
		repeat with win in windows
			repeat with t in tabs of win
				tell t
					if URL starts with "https://www.youtube.com/watch" then
						set state to do JavaScript [�
							"document", �
							".querySelectorAll('div[class*=\"-mode\"]')[0]", �
							".className", �
							".match(/(playing|paused|ended)-mode/)[1]"] �
							as text
						if state = "playing" then
							--set current tab of win to t
							set index of win to 1
							set current tab of win to t
							--tell application "Safari" to set win to front
							activate
							--set t of win to 1
							return true
						end if
					end if
				end tell
			end repeat
		end repeat
	end tell
	return false
end if