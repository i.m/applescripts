-- tell application "System Events"
  set microVolume to input volume of (get volume settings)
  if microVolume > 0 then
    --your code
    set volume input volume 0
  else
    --your code
    set volume input volume 100
  end if
-- end