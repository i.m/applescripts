set displayTitle to "" as string
if application "Spotify" is running then
	tell application "Spotify"
		if player state is playing then
			set artistTitle to ((get artist of current track) & " - " & (get name of current track))
			if length of artistTitle is less than 15 then
				set displayTitle to artistTitle
			else
				set displayTitle to (get name of current track)
				if length of displayTitle is greater than 15 then
					set displayTitle to text 1 thru 15 of (get name of current track) & "..."
				end if
			end if
			set artworkURL to artwork url of current track
			do shell script "curl " & artworkURL & " -o ~/Library/Application\\ Support/BetterTouchTool/spotify_cover.png"
			set fileName to ((((path to application support folder from user domain) as text) & "BetterTouchTool:" as text) & "spotify_cover.png")
			return "{\"text\":\"" & displayTitle & "\", \"icon_path\":\"" & (POSIX path of fileName as text) & "\" }"
			return displayTitle
		else
			return "Paused"
		end if
	end tell
end if
return str
